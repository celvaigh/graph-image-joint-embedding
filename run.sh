#!/bin/bash

dim=128
for att in "type" "time" "school" "author" 
do
    python main.py  --mode train --model kgm --dir_dataset ../SemArt --workers 40 --att $att --graph_dim $dim #--encoder vae
    exit
done