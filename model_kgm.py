import torch.nn as nn
from torchvision import models
import torch
from torch.nn import functional as F
from gensim.models import Word2Vec
import tqdm
import numpy as np
import pdb


class Embeddings(nn.Module):
    def __init__(self, args_dic):
        super(Embeddings, self).__init__()  # Calling Super Class's constructor
        self.args_dic=args_dic
        self.graphEm = Word2Vec.load(args_dic.graph_embs)
        ent2int = np.load("Data/ents2int.npy",allow_pickle=True).item()
        # pdb.set_trace()
        self.oov=0
        int2ent = {ent2int[key]:key for key in tqdm.tqdm(ent2int)}
        
        n2v_vectors = [ self.get_init_embed(int2ent[e].encode('Cp1252')) for e in tqdm.tqdm(range(args_dic.entities))]

        print("oov : {}".format(self.oov))

        # rel2int=np.load("Data/rels2int.npy",allow_pickle=True).item()
        self.entity_embedding = nn.Embedding(args_dic.entities, args_dic.graph_dim)
        self.entity_embedding.weight.data = torch.FloatTensor(n2v_vectors)
        self.relation_embedding = nn.Embedding(args_dic.relations, args_dic.graph_dim)

    def get_init_embed(self,x):
        try: return self.graphEm[x]
        except: 
            self.oov += 1
            return np.random.normal(0, 1, self.args_dic.graph_dim)
    # for vector of elements
    def get_vectorised_values_entity(self, x):
        a = self.entity_embedding(torch.LongTensor(x))
        return a

    def get_vectorised_values_relation(self, x):
        a = self.relation_embedding(torch.LongTensor(x))
        return a

    def get_vectorised_value_relation(self, x):
        a = self.relation_embedding(x)
        return a

    def get_vectorised_value_entity(self, x):
        a = self.entity_embedding(x)
        return a

# https://github.com/pytorch/examples/blob/master/vae/main.py
class VAE(nn.Module):
    def __init__(self,grap_dim):
        super(VAE, self).__init__()

        self.fc1 = nn.Linear(2048, 512)
        self.fc21 = nn.Linear(512, grap_dim)
        self.fc22 = nn.Linear(512, grap_dim)
        self.fc3 = nn.Linear(grap_dim, 512)
        self.fc4 = nn.Linear(512, 2048)


    def encode(self, x):
        h1 = F.relu(self.fc1(x))
        return self.fc21(h1), self.fc22(h1)

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5*logvar)
        eps = torch.randn_like(std)
        return mu + eps*std

    def decode(self, z):
        h3 = F.relu(self.fc3(z))
        return torch.sigmoid(self.fc4(h3))

    def forward(self, x):
        mu, logvar = self.encode(x.view(-1, 2048))
        z = self.reparameterize(mu, logvar)
        self.decode(z)
        return z

class KGM(nn.Module):
    # Inputs an image and ouputs the prediction for the class and the projected embedding into the graph space

    def __init__(self, num_class,args_dict):
        super(KGM, self).__init__()

        # Load pre-trained visual model
        resnet = models.resnet50(pretrained=True)
        self.resnet = nn.Sequential(*list(resnet.children())[:-1])

        # Classifier
        self.classifier = nn.Sequential(nn.Linear(2048, num_class))

        # Graph space encoder
        if not len(args_dict.encoder):
            self.nodeEmb = nn.Sequential(nn.Linear(2048, args_dict.graph_dim))#2048-->grap_dim
        else:
            self.nodeEmb =VAE(args_dict.graph_dim)



    def forward(self, img):

        visual_emb = self.resnet(img)
        visual_emb = visual_emb.view(visual_emb.size(0), -1)
        pred_class = self.classifier(visual_emb)
        graph_proj = self.nodeEmb(visual_emb)

        return [pred_class, graph_proj]