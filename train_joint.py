from __future__ import division

import os
import torch
import torch.nn as nn
import torch.utils.data
from torchvision import transforms
import numpy as np

from utils import AverageMeter
from model_kgm import KGM,Embeddings
from dataloader_joint import ArtDatasetKGM
from attributes import load_att_class

def marginLoss(args_dict, p_score, n_score):
    criterion = nn.MarginRankingLoss(args_dict.margin, False).cuda()
    y = torch.autograd.Variable(torch.Tensor([-1])).cuda()
    loss = criterion(p_score, n_score, y)
    return loss
def print_classes(type2idx, school2idx, timeframe2idx, author2idx):
    print('Att type\t %d classes' % len(type2idx))
    print('Att school\t %d classes' % len(school2idx))
    print('Att time\t %d classes' % len(timeframe2idx))
    print('Att author\t %d classes' % len(author2idx))


def save_model(args_dict, state,graphE):
    directory = args_dict.dir_model #+ "%s/"%(args_dict.name)
    """if not os.path.exists(directory):
        os.makedirs(directory)"""
    if args_dict.randomize:alias=alias="{}-".format(os.getpid())
    else:alias=""
    filename = "{}/best-joint-kgm-{}{}-{}-{}model.pth.tar".format(args_dict.dir_model,args_dict.kernel,args_dict.graph_dim,args_dict.att,alias)#directory + 'best_model.pth.tar'
    args_dict.model_path=filename
    torch.save(state, filename)
    filename="{}/best-joint-KGE-{}-{}-model.pth.tar".format(args_dict.dir_model,args_dict.graph_dim,args_dict.att)
    torch.save(graphE,filename)


def resume(args_dict, model, optimizer):

    best_val = float(0)
    args_dict.start_epoch = 0
    if args_dict.resume:
        if os.path.isfile(args_dict.resume):
            print("=> loading checkpoint '{}'".format(args_dict.resume))
            checkpoint = torch.load(args_dict.resume)
            args_dict.start_epoch = checkpoint['epoch']
            best_val = checkpoint['best_val']
            model.load_state_dict(checkpoint['state_dict'])
            try:optimizer.load_state_dict(checkpoint['optimizer'])
            except:pass
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args_dict.resume, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args_dict.resume))
            best_val = float(0)

    return best_val, model, optimizer


def trainEpoch(args_dict, train_loader, model,graphE, criterion, optimizer, epoch):

    # object to store & plot the losses
    losses = AverageMeter()

    # switch to train mode
    model.train()
    torch.cuda.empty_cache()
    for batch_idx, (input, target) in enumerate(train_loader):

        # Inputs to Variable type
        input_var = list()
        for j in range(len(input)):
            input_var.append(torch.autograd.Variable(input[j]).cuda())

        # Targets to Variable type
        target_var = list()
        
        target[0] = target[0].cuda(async=True)
        target_var.append(torch.autograd.Variable(target[0]))

        target_x=graphE.get_vectorised_value_entity(target[-1])
        target_x = target_x.cuda(async=True)
        target_x=torch.autograd.Variable(target_x)

        #Getting the vectors for the triples where the image is involved
        x_train,negative_train=target[1].data.numpy(),target[2].data.numpy()
        #print(x_train.shape,np.array([x[:,2] for x in x_train]))
        h = graphE.get_vectorised_values_entity(np.array([x[:,0] for x in x_train]))
        t = graphE.get_vectorised_values_entity(np.array([x[:,1] for x in x_train]))
        r = graphE.get_vectorised_values_relation(np.array([x[:,2] for x in x_train]))
        #Negative sampling
        h_negative = graphE.get_vectorised_values_entity(np.array([x[:,0] for x in negative_train]))
        t_negative = graphE.get_vectorised_values_entity(np.array([x[:,1] for x in negative_train]))
        r_negative = graphE.get_vectorised_values_relation(np.array([x[:,2] for x in negative_train]))
        h,r,t=h.cuda(async=True),r.cuda(async=True),t.cuda(async=True)
        h,r,t=torch.autograd.Variable(h),torch.autograd.Variable(r),torch.autograd.Variable(t)
        h_negative=h_negative.cuda(async=True)
        r_negative=r_negative.cuda(async=True)
        t_negative=t_negative.cuda(async=True)
        h_negative=torch.autograd.Variable(h_negative)
        r_negative=torch.autograd.Variable(r_negative)
        t_negative=torch.autograd.Variable(t_negative)

        # free some memory
        # for p in model.parameters():
        #     if p.grad is not None:del p.grad  
        torch.cuda.empty_cache()
        # print(len(input_var),input_var[0].shape)
        # Output of the model
        output = model(input_var[0])

        class_loss = criterion[0](output[0], target_var[0])
        if args_dict.L1_Norm:
            score_pos = torch.norm((h + r - t), p=1, dim=1)
            score_neg = torch.norm((h_negative + r_negative - t_negative), p=1, dim=1)
        else:
            score_pos = torch.norm((h + r - t), p=2, dim=1)
            score_neg = torch.norm((h_negative + r_negative - t_negative), p=2, dim=1)
            
        transe_loss =   marginLoss(args_dict,score_pos, score_neg)
        encoder_loss = criterion[1](output[1], target_x)
        train_loss = args_dict.lambda_c * class_loss + \
                        args_dict.lambda_e * encoder_loss + \
                        args_dict.lambda_t * transe_loss
        losses.update(train_loss.data.cpu().numpy(), input[0].size(0))

        # Backpropagate loss and update weights
        optimizer.zero_grad()
        train_loss.backward()
        optimizer.step()

        #Normalize entities and relation to sum to 1
        graphE.entity_embedding.weight.data.renorm_(p=2, dim=0, maxnorm=1)
        graphE.relation_embedding.weight.data.renorm_(p=2, dim=0, maxnorm=1)
        

        # Print info
        print('Train Epoch: {} [{}/{} ({:.0f}%)]\t'
              'Loss {loss.val:.4f} ({loss.avg:.4f})\t'.format(
            epoch, batch_idx, len(train_loader), 100. * batch_idx / len(train_loader), loss=losses))

def valEpoch(args_dict, val_loader, model, criterion, epoch):

    # object to store & plot the losses
    losses = AverageMeter()

    # switch to evaluation mode
    model.eval()
    for batch_idx, (input, target) in enumerate(val_loader):

        # Inputs to Variable type
        input_var = list()
        for j in range(len(input)):
            input_var.append(torch.autograd.Variable(input[j]).cuda())

        # Targets to Variable type
        target_var = list()
        for j in range(len(target)):
            target[j] = target[j].cuda(async=True)
            target_var.append(torch.autograd.Variable(target[j]))

        # Predictions
        with torch.no_grad():
            output = model(input_var[0])
        
        _, predicted = torch.max(output[0], 1)
        train_loss = criterion[0](output[0], target_var[0])
        losses.update(train_loss.data.cpu().numpy(), input[0].size(0))

        # Save predictions to compute accuracy
        if batch_idx==0:
            out = predicted.data.cpu().numpy()
            label = target[0].cpu().numpy()
        else:
            out = np.concatenate((out,predicted.data.cpu().numpy()),axis=0)
            label = np.concatenate((label,target[0].cpu().numpy()),axis=0)

    # Accuracy
    acc = np.sum(out == label) / len(out)

    # Print validation info
    print('Validation set: Average loss: {:.4f}\t'
          'Accuracy {acc}'.format(losses.avg, acc=acc))

    # Return acc
    return acc


def train_knowledgegraph_classifier(args_dict):

    # Load classes
    type2idx, school2idx, time2idx, author2idx = load_att_class(args_dict)
    if args_dict.att == 'type':
        att2i = type2idx
    elif args_dict.att == 'school':
        att2i = school2idx
    elif args_dict.att == 'time':
        att2i = time2idx
    elif args_dict.att == 'author':
        att2i = author2idx

    # Data transformation for training (with data augmentation) and validation
    train_transforms = transforms.Compose([
        transforms.Resize(256),                             # rescale the image keeping the original aspect ratio
        transforms.CenterCrop(256),                         # we get only the center of that rescaled
        transforms.RandomCrop(224),                         # random crop within the center crop (data augmentation)
        transforms.RandomHorizontalFlip(),                  # random horizontal flip (data augmentation)
        transforms.ToTensor(),                              # to pytorch tensor
        transforms.Normalize(mean=[0.485, 0.456, 0.406, ],  # ImageNet mean substraction
                             std=[0.229, 0.224, 0.225])
    ])

    val_transforms = transforms.Compose([
        transforms.Resize(256),                             # rescale the image keeping the original aspect ratio
        transforms.CenterCrop(224),                         # we get only the center of that rescaled
        transforms.ToTensor(),                              # to pytorch tensor
        transforms.Normalize(mean=[0.485, 0.456, 0.406, ],  # ImageNet mean substraction
                             std=[0.229, 0.224, 0.225])
    ])

     # Dataloaders for training and validation
    train_loader = torch.utils.data.DataLoader(
        ArtDatasetKGM(args_dict, set = 'train', att2i=att2i, att_name=args_dict.att, transform = train_transforms),
        batch_size=args_dict.batch_size, shuffle=True, pin_memory=True, num_workers=args_dict.workers)
    print('Training loader with %d samples' % train_loader.__len__())

    val_loader = torch.utils.data.DataLoader(
        ArtDatasetKGM(args_dict, set = 'val', att2i=att2i, att_name=args_dict.att, transform = val_transforms),
        batch_size=args_dict.batch_size, shuffle=True, pin_memory=True, num_workers=args_dict.workers)
    print('Validation loader with %d samples' % val_loader.__len__())

    # Define model
    model = KGM(len(att2i),args_dict)

    graphE=Embeddings(args_dict)
    if args_dict.use_gpu:
        #model.cuda()
        if torch.cuda.device_count() > 1:
            ng=torch.cuda.device_count()
            print("Let's use", ng, "GPUs!")
            model = nn.DataParallel(model,device_ids =[0])
            model.cuda()
            #model = nn.DataParallel(model)
        else:
            model.cuda()
        #graphE.cuda()

    # Loss and optimizer
    class_loss = nn.CrossEntropyLoss().cuda()
    encoder_loss = nn.SmoothL1Loss()
    loss = [class_loss, encoder_loss]
    model_params=list(filter(lambda p: p.requires_grad, model.parameters()))
    embedding_params=list(filter(lambda p: p.requires_grad, graphE.parameters()))

    optimizer = torch.optim.SGD(model_params+embedding_params, lr=args_dict.lr, momentum=args_dict.momentum)
    # Resume training if needed
    best_val, model, optimizer = resume(args_dict, model, optimizer)

    # Now, let's start the training process!
    print('Start training KGM model...')
    pat_track = 0
    for epoch in range(args_dict.start_epoch, args_dict.nepochs):

        # Compute a training epoch
        trainEpoch(args_dict, train_loader, model,graphE, loss, optimizer, epoch)

        # Compute a validation epoch
        accval = valEpoch(args_dict, val_loader, model, loss, epoch)

        # check patience
        if accval <= best_val:
            pat_track += 1
        else:
            pat_track = 0
        if pat_track >= args_dict.patience:
            break

        # save if it is the best model
        is_best = accval > best_val
        best_val = max(accval, best_val)
        if is_best:
            save_model(args_dict, {
                'epoch': epoch + 1,
                'state_dict': model.state_dict(),
                'best_val': best_val,
                'optimizer': optimizer.state_dict(),
                'valtrack': pat_track,
                'curr_val': accval,
            },graphE)
        print '** Validation: %f (best acc) - %f (current acc) - %d (patience)' % (best_val, accval, pat_track)

def run_train(args_dict):

    # Set seed for reproducibility
    torch.manual_seed(args_dict.seed)
    if args_dict.use_gpu:
        torch.cuda.manual_seed(args_dict.seed)
    train_knowledgegraph_classifier(args_dict)
   

