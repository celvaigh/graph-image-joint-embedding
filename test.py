from __future__ import division

import numpy as np
import torch
from torchvision import transforms

from model_kgm import KGM
from dataloader_joint import ArtDatasetKGM
from attributes import load_att_class
import tqdm,os
import pandas as pd


def test_knowledgegraph(args_dict):

    # Load classes
    type2idx, school2idx, time2idx, author2idx = load_att_class(args_dict)
    if args_dict.att == 'type':
        att2i = type2idx
    elif args_dict.att == 'school':
        att2i = school2idx
    elif args_dict.att == 'time':
        att2i = time2idx
    elif args_dict.att == 'author':
        att2i = author2idx

    model = KGM(len(att2i),args_dict)
    if args_dict.use_gpu:
        model.cuda()

    # Load best model
    print("=> loading checkpoint '{}'".format(args_dict.model_path))
    checkpoint = torch.load(args_dict.model_path)
    args_dict.start_epoch = checkpoint['epoch']
    model_state_dict = torch.load(args_dict.model_path)
    saved_weights = model_state_dict.keys()
    print(model_state_dict['epoch'], model_state_dict['best_val'], model_state_dict['valtrack'], model_state_dict['curr_val'])
    #exit()
    model.load_state_dict(checkpoint['state_dict'])
    print("=> loaded checkpoint '{}' (epoch {})"
          .format(args_dict.model_path, checkpoint['epoch']))

    # Data transformation for test
    test_transforms = transforms.Compose([
        transforms.Resize(256),                             # rescale the image keeping the original aspect ratio
        transforms.CenterCrop(224),                         # we get only the center of that rescaled
        transforms.ToTensor(),                              # to pytorch tensor
        transforms.Normalize(mean=[0.485, 0.456, 0.406, ],  # ImageNet mean substraction
                             std=[0.229, 0.224, 0.225])
    ])

    # Data Loaders for test
    test_loader = torch.utils.data.DataLoader(
        ArtDatasetKGM(args_dict, set='test', att2i=att2i, att_name=args_dict.att, transform=test_transforms),
        batch_size=args_dict.batch_size, shuffle=False, pin_memory=(not args_dict.no_cuda), num_workers=args_dict.workers)

    # Switch to evaluation mode & compute test samples embeddings
    model.eval()

    for i, (input, target) in enumerate(test_loader):

        # Inputs to Variable type
        input_var = list()
        for j in range(len(input)):
            input_var.append(torch.autograd.Variable(input[j]).cuda())

        # Targets to Variable type
        target_var = list()
        for j in range(len(target)):
            target[j] = target[j].cuda(async=True)
            target_var.append(torch.autograd.Variable(target[j]))

        # Output of the model
        with torch.no_grad():
            output = model(input_var[0])
            outsoftmax = torch.nn.functional.softmax(output[0])
        conf, predicted = torch.max(outsoftmax, 1)

        # Store embeddings
        if i==0:
            out = predicted.data.cpu().numpy()
            label = target[0].cpu().numpy()
            scores = conf.data.cpu().numpy()
        else:
            out = np.concatenate((out,predicted.data.cpu().numpy()),axis=0)
            label = np.concatenate((label,target[0].cpu().numpy()),axis=0)
            scores = np.concatenate((scores, conf.data.cpu().numpy()),axis=0)

    # Compute Accuracy
    acc = np.sum(out == label)/len(out)
    print('Model %s\tTest Accuracy %.03f' % (args_dict.model_path, acc))



def run_test(args_dict):

    test_knowledgegraph(args_dict)
   