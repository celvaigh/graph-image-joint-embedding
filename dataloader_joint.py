import torch
import torch.nn as nn
import torch.utils.data as data
import os
from PIL import Image
import pandas as pd
import numpy as np
from gensim.models import Word2Vec
from gensim.models import KeyedVectors
from utils import graph2rdf
import tqdm
class ArtDatasetKGM(data.Dataset):

    def __init__(self, args_dict, set, att2i, att_name, transform = None):
        """
        Args:
            args_dict: parameters dictionary
            set: 'train', 'val', 'test'
            att2i: attribute vocabulary
            att_name: either 'type', 'school', 'time', or 'author'
            transform: data transform
        """

        self.args_dict = args_dict
        self.set = set
        
        # Load Data + Graph Embeddings
        if self.set == 'train':
            textfile = os.path.join(args_dict.dir_dataset, args_dict.csvtrain)
            try:
                self.image2triples=np.load("Data/ent2triples.npy",allow_pickle=True).item()
                self.ent2int=np.load("Data/ents2int.npy",allow_pickle=True).item()
                self.rel2int=np.load("Data/rels2int.npy",allow_pickle=True).item()
            except:
                make_triples(args_dict)
                self.image2triples=np.load("Data/ent2triples.npy",allow_pickle=True).item()
                self.ent2int=np.load("Data/ents2int.npy",allow_pickle=True).item()
                self.rel2int=np.load("Data/rels2int.npy",allow_pickle=True).item()
            args_dict.entities=len(self.ent2int)
            args_dict.relations=len(self.rel2int)
            print("KG entities:{}, relations:{}".format(args_dict.entities,args_dict.relations))

        elif self.set == 'val':
            textfile = os.path.join(args_dict.dir_dataset, args_dict.csvval)
        elif self.set == 'test':
            textfile = os.path.join(args_dict.dir_dataset, args_dict.csvtest)
        df = pd.read_csv(textfile, delimiter='\t', encoding='Cp1252')

        self.imagefolder = os.path.join(args_dict.dir_dataset, args_dict.dir_images)
        self.transform = transform
        self.att2i = att2i
        self.imageurls = list(df['IMAGE_FILE'])
        if att_name == 'type':
            self.att = list(df['TYPE'])
        elif att_name == 'school':
            self.att = list(df['SCHOOL'])
        elif att_name == 'time':
            self.att = list(df['TIMEFRAME'])
        elif att_name == 'author':
            self.att = list(df['AUTHOR'])
        self.get_negative_samples = np.vectorize(self.get_negative_sample, signature='(),(),()->(n)')


    def __len__(self):
        return len(self.imageurls)

    def get_negative_sample(self, h, t, r):
        # randomly corrupt head and tails
        # X1 = [h, r, t]
        pos = np.random.randint(10, size=1)[0]
        curr_entity = np.random.randint(len(self.ent2int), size=1)[0]
        
        #curr_entity = curr_entity[1] # now I only get the index , so only one column
        if (pos < 5):
            X2 = [h, curr_entity, r]
        else:
            X2 = [curr_entity, t, r]
        # x = np.array([X1, X2])
        # X2 = torch.tensor([X2])
        X2 = np.array(X2)
        # print X2
        # print
        return X2
    
    def class_from_name(self, vocab, name):

        if vocab.has_key(name):
            idclass= vocab[name]
        else:
            idclass = vocab['UNK']

        return idclass


    def __getitem__(self, index):
        """Returns data sample as a pair (image, description)."""

        # Load image & apply transformation
        imagepath = self.imagefolder + self.imageurls[index]
        image = Image.open(imagepath).convert('RGB')
        if self.transform is not None:
            image = self.transform(image)

        # Attribute class
        idclass = self.class_from_name(self.att2i, self.att[index])

        # Graph embedding (only training samples)
        if self.set == 'train':
            x_train = self.image2triples[self.ent2int[self.imageurls[index]]]
            #print(self.imageurls[index],x_train)
            x_train_negative =self.get_negative_samples(x_train[:, 0], x_train[:, 1], x_train[:, 2])
            return [image], [idclass,x_train,x_train_negative,self.ent2int[self.imageurls[index]]]
        else:
            return [image], [idclass]

def make_triples(args_dict):

    dfkg = pd.read_csv(args_dict.kg_triples, delimiter='\t', encoding='Cp1252',header=None)
    subjects=dfkg[0]
    links=dfkg[1]
    objects=dfkg[2]
    n_triples=len(dfkg)
    eid,rid={},{}
    ne,nr=0,0
    triples={}
    for i in tqdm.tqdm(range(n_triples)):
        s,r,o=subjects[i].split("/")[-1],links[i].split("/")[-1],objects[i].split("/")[-1]
        try:_=eid[s]
        except:eid[s]=ne;ne+=1
        try:_=eid[o]
        except:eid[o]=ne;ne+=1
        try:_=rid[r]
        except:rid[r]=nr;nr+=1
        
        try:triples[eid[s]]+=[np.array([eid[s],eid[o],rid[r]])]
        except:triples[eid[s]]=[np.array([eid[s],eid[o],rid[r]])]

        try:triples[eid[o]]+=[np.array([eid[s],eid[o],rid[r]])]
        except:triples[eid[o]]=[np.array([eid[s],eid[o],rid[r]])]
    dic={}
    for i in tqdm.tqdm(triples):dic[i]=np.array(triples[i])
    np.save("Data/ents2int.npy",eid)
    np.save("Data/rels2int.npy",rid)
    np.save("Data/ent2triples.npy",dic)