import warnings
warnings.filterwarnings("ignore")

from params import get_parser
from train_joint import run_train
from test import run_test
from utils import randomizer

import os
if __name__ == "__main__":

    # Load parameters
    parser = get_parser()
    args_dict, unknown = parser.parse_known_args()
    assert args_dict.att in ['type', 'school', 'time', 'author'], \
        'Incorrect classifier. Please select type, school, time, or author.'

    if args_dict.model == 'mtl':
        args_dict.att = 'all'
    args_dict.name = '{}-{}'.format(args_dict.model, args_dict.att)
    
    #args_dict.model_path='Models/best-kgm-transe-{}-model.pth.tar'.format(args_dict.att)
    # Check mode and model are correct
    assert args_dict.mode in ['train', 'test'], 'Incorrect mode. Please select either train or test.'
    assert args_dict.model in ['mtl', 'kgm'], 'Incorrect model. Please select either mlt or kgm.'

    args_dict.randomize=False
    args_dict.resume='/nfs/nas4/cicoda_tmp/cicoda_tmp/Osaka/context-art-classification/Models/Noa/best-kgm-{}-model.pth.tar'.format(args_dict.att)
    if args_dict.randomize:
        randomizer(args_dict) 
        
    opts = vars(args_dict)
    print('------------ Options -------------')
    for k, v in sorted(opts.items()):
        print('%s: %s' % (str(k), str(v)))
    print('-----------------------------------')

    # Run process
    run_train(args_dict)
    run_test(args_dict)

    #Clean random splitted files
    if args_dict.randomize:
        if os.path.exists(os.path.join(args_dict.dir_dataset, args_dict.csvval)) and os.path.exists(os.path.join(args_dict.dir_dataset, args_dict.csvtest)):
            os.remove(os.path.join(args_dict.dir_dataset, args_dict.csvval))
            os.remove(os.path.join(args_dict.dir_dataset, args_dict.csvtest))
        if os.path.exists(args_dict.model_path):
            os.remove(args_dict.model_path)
    