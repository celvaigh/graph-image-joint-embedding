#!/bin/bash
#OAR -n Align entities

#OAR -l {host ='igrida-abacus9.irisa.fr'}/nodes=1/gpu_device=1,walltime=48:00:00
#OAR -p virt='YES'
#OAR -O igrida.%jobid%.output
#OAR -E igrida.%jobid%.output
# 
# {host ='igrida-abacus9.irisa.fr'}
. /etc/profile.d/modules.sh
dim=128
for i in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
do
   for att in "type" "time" "school" "author" 
   do
     python main.py  --mode train --model kgm --dir_dataset ../SemArt --workers 40 --att $att --graph_dim $dim --batch_size 128 #--encoder vae
   done
done