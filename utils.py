import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
from sklearn import cluster, datasets
from sklearn.neighbors import kneighbors_graph
from sklearn.preprocessing import StandardScaler
# import umap.umap_ as UMAP
# import seaborn as sns
# import hdbscan


from gensim.models import Word2Vec
import numpy as np
from gensim.models import KeyedVectors
import tqdm 
import matplotlib
matplotlib.use('Agg')
import rdflib
import tqdm
import os
from rdflib import URIRef, BNode, Literal, XSD
from rdflib.namespace import RDF, FOAF
from params import get_parser
import pandas as pd
import pickle

############################# Plotting #######################################
def hdbscan_plot(path,name):    
    tokens = []
    with open(path+name+"/entity2vec.vec") as f:
        vectors=[np.array(x.split("\t")[:-1]) for x in f.read().split("\n")]
    
    with open(path+"entity2id.txt") as f:
        imgIds=f.read().split("\n")

    model=[str(len(imgIds)-1)+" "+str(len(vectors[0]))]
    for l in tqdm.tqdm(imgIds):
        try:
            ID,im=l.split("\t")
            url=im.split("/")[-1]
            if ".jpg" not in url:continue
        except:continue
        if not len(vectors[int(ID)]):continue
        tokens.append(vectors[int(ID)])
    print("number of image:{}".format(len(tokens)))
    #color_palette = sns.color_palette('deep', 8)
    #cluster_colors = [color_palette[x] if x >= 0 else (0.5, 0.5, 0.5) for x in clusterer.labels_]
    #cluster_member_colors = [sns.desaturate(x, p) for x, p in zip(cluster_colors, clusterer.probabilities_)]
    model = UMAP.UMAP(n_neighbors = 15, min_dist = 0.0, n_components = 2,a=1.5,b=2,verbose = True)
    p_umap = model.fit_transform(tokens)
    clusterer = hdbscan.HDBSCAN(min_cluster_size=15).fit(p_umap)
    print("finished clustering with HDBSCAN ")
    plt.figure(figsize=(10,10))
    plt.scatter(p_umap[:, 0], p_umap[:, 1],c=clusterer.labels_,edgecolors='gray',cmap='Spectral')
    plt.axis('off')
    plt.savefig(path+name+"/semart_umap_hdbscan.png")


##################  Graph pre-processing #####################
def graph_to_idx(filename,output):
    ids={}
    with open(filename) as f:
        data=f.read().split("\n")
    out=[]
    idx=0
    for edge in tqdm.tqdm(data):
        try:s,o=edge.split("\t")
        except:print(edge);continue
        
        try:sid=ids[s]
        except:
            ids[s]=idx
            sid=idx
            idx+=1
        try:oid=ids[o]
        except:
            ids[o]=idx
            oid=idx
            idx+=1
        out+=["\t".join([str(sid),str(oid)])]

    with open(output,"w") as f:
        f.write("\n".join(out))
    print('ids:{} vs{}'.format(len(ids),idx))
    np.save("../SemArt/ids2node.npy",ids)

def get_node2idx():
    """
    Transform nodes to ids used in the training of node2vec
    """
    try:
        ids=np.load("../SemArt/node2ids.npy").item()
    except:
        ids=np.load("../SemArt/ids2node.npy").item()
        ids={ids[i]:i for i in ids}
        np.save("../SemArt/node2ids.npy",ids)
    return ids

def convert_graph_idx_to_str(filename,output):
    with open(filename) as f:
        data=f.read().split("\n")
    print("vocab size:{}".format(len(data)))
    ids=get_node2idx()
    #test the WV model
    try:
        model=KeyedVectors.load_word2vec_format(filename , binary=False,unicode_errors='ignore')
        print(len(model.wv.vocab))
    except Exception as e:
        print(e,filename);exit()

    with open(output,"w") as f:
        #vocab dim
        f.write(str(len(model.wv.vocab))+" "+str(model.wv.vector_size) +"\n")
        for entity in tqdm.tqdm(model.wv.vocab):
            #node vector
            #line=entity.split(" ")
            try:
                word=ids[int(entity)]
                v=model.wv[entity]
            except Exception as e:print(e);continue
            l=word+" "+" ".join(map(str,v))+"\n"
            f.write(l)
    try:
        model=KeyedVectors.load_word2vec_format(output , binary=False,unicode_errors='ignore')
        print(len(model.wv.vocab))
    except Exception as e:
        print(e,filename);exit()
    print("done!")

def graph2rdf():
    parser = get_parser()
    args_dict, unknown = parser.parse_known_args()
    g = rdflib.Graph()
    df=pd.read_csv(args_dict.dir_dataset+"/semart_train_kw.csv",delimiter='\t', encoding='Cp1252')
    files=df["IMAGE_FILE"]
    authors=df["AUTHOR"]
    titles=df["TITLE"]
    types=df["TYPE"]
    dates=df["DATE"]
    descriptions=df["DESCRIPTION"]
    schools=df["SCHOOL"]
    timeframes=df["TIMEFRAME"]
    ontology="http://art.org/ontology/"
    root="http://art.org/ressources/"
    artwork=root+"artwork"

    keywords = df['KEYWORDS_NGRAMS']
    technique = df['TECHNIQUE']
    # Split technique into support and material
    material = technique.str.split(',').str[0]

    if not os.path.exists("Results"):
        os.makedirs("Results")
    for i in tqdm.tqdm(range(len(files))):
        g.add((URIRef(root+files[i]),RDF.type,URIRef(artwork)))
        g.add((URIRef(root+files[i]),URIRef(ontology+"label"),Literal(titles[i])))
        #g.add((URIRef(root+files[i]),URIRef(ontology+"description"),Literal(descriptions[i])))
        g.add((URIRef(root+files[i]),URIRef(ontology+"hasSchool"),Literal(schools[i])))
        #g.add((URIRef(authors[i]),RDF.type,FOAF.Person))
        #g.add((URIRef(authors[i]),FOAF.name,Literal(authors[i])))
        if 'UNKNOWN' not in authors[i] :
            g.add((URIRef(root+files[i]),URIRef(ontology+"hasAuthor"),Literal(authors[i])))
        #dtt = Literal(dates[i],datatype=XSD.date)
        if dates[i]!='-':
            g.add((URIRef(root+files[i]),URIRef(ontology+"created"),Literal(dates[i])))
        g.add((URIRef(root+files[i]),URIRef(ontology+"hasPeriod"),Literal(timeframes[i])))
        g.add((URIRef(root+files[i]),URIRef(ontology+"hasType"),Literal(types[i])))
        if len(keywords[i]):
            g.add((URIRef(root+files[i]),URIRef(ontology+"KEYWORDS_NGRAMS"),Literal(keywords[i])))
        if len(material[i]):
            g.add((URIRef(root+files[i]),URIRef(ontology+"hasMaterial"),Literal(material[i])))
        #if i>10:break


    print("triples in g {}".format(len(g)))
    x=getall(g)
    with open('semart_rdf_nsupport.csv',"w") as f:f.write("\n".join(x))
    #g.serialize(destination='semart_rdf.nt', format='nt')
def getall(g):
    query = """
    select distinct ?s ?r ?o
    where { ?s ?r ?o}"""
    res=g.query(query)
    result=[]
    for row in res:
        try:s,r,o=str(row[0]),str(row[1]),str(row[2].encode("Cp1252"))
        except:print(row)
        result+=["\t".join([s,r,o])]
    return result
def process(filename):
    with open(filename) as f:
        data=f.read().split("\n")

    ds,dr={},{}
    train=[]
    for line in data:
        try:s,r,o=line.split("\t")
        except:
            print(line.split(","))
        #o="_".join(o.split(" "))
        try:tmp=ds[s]
        except:ds[s]=len(ds)
        try:tmp=dr[r]
        except:dr[r]=len(dr)
        try:tmp=ds[o]
        except:ds[o]=len(ds)
        train+=[" ".join(map(str,[ds[s],ds[o],dr[r]]))]
        
    entities=[str(ds[i])+"\t"+i for i in ds]
    relations=[str(dr[i])+"\t"+i for i in dr]
        
    with open("relation2id.txt","w") as f:f.write("\n".join(map(str,[len(dr)]+relations)))
    with open("entity2id.txt","w") as f:f.write("\n".join(map(str,[len(ds)]+entities)))
    with open("train2id.txt","w") as f:f.write("\n".join([str(len(train))]+train))


def randomizer(args_dict,size=1070):
    tic=time.time()
    textfile=os.path.join(args_dict.dir_dataset, args_dict.csvtest)
    dftest = pd.read_csv(textfile, delimiter='\t', encoding='Cp1252')
    textfile=os.path.join(args_dict.dir_dataset, args_dict.csvval)
    dfval = pd.read_csv(textfile, delimiter='\t', encoding='Cp1252')
    #df=dftest+dfval
    frames = [dftest,dfval]
    df = pd.concat(frames)

    test_idx=[]
    possible_idx=range(len(df))
    print("processing df with {} from dfval {} and dftest {}".format(len(possible_idx),len(dfval),len(dftest)))
    while len(test_idx)<size-1:
        x=random.choice(possible_idx)
        if x not in test_idx:test_idx+=[x]
    val_idx=[i for i in possible_idx if i not in test_idx]
    dftest=df.iloc[test_idx]
    dfval=df.iloc[val_idx]
    pid = os.getpid() 
    print("sampled {} dev, and {} test".format(len(dfval),len(dftest)))
    dfval.to_csv(os.path.join(args_dict.dir_dataset, "random_{}_val.csv".format(pid)),sep='\t', encoding='Cp1252')
    dftest.to_csv(os.path.join(args_dict.dir_dataset, "random_{}_test.csv".format(pid)),sep='\t', encoding='Cp1252')
    toc=time.time()-tic
    args_dict.csvval="random_{}_val.csv".format(pid)
    args_dict.csvtest="random_{}_test.csv".format(pid)

    if args_dict.randomize:alias="{}-".format(pid)
    else:alias=""
    args_dict.model_path="{}/best-joint-kgm-{}{}-{}model.pth.tar".format(args_dict.dir_model,args_dict.kernel,args_dict.graph_dim,args_dict.att)#directory + 'best_model.pth.tar'
    print("sampling time:{}".format(toc))

################# Losses ###################################
class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def save_obj(obj, filename):
    f = open(filename, 'wb')
    pickle.dump(obj, f)
    f.close()
    print("Saved object to %s." % filename)


def load_obj(filename):
    f = open(filename, 'rb')
    obj = pickle.load(f)
    f.close()
    return obj

def main():
    process("semart_rdf.csv")
if __name__ == "__main__":main()